﻿import ecdtools
import sys
# from decimal import Decimal
from scipy.interpolate import interp1d
# import matplotlib.pyplot as plt

class Spline:
    # wrapper for scipy.interpolate.interp1d that
    #  * returns a float value
    #  * returns y(xmin) for x<xmin
    #  * returns y(xmax) for x>xmax

    def __init__(self, x, y, copy=True, kind='cubic'):
        self.spline = interp1d(x=x, y=y, copy=copy, kind=kind)
        self.xmin = min(x)
        self.xmax = max(x)
        
    def __call__(self, x):
        return float(self.spline(min(max(x,self.xmin),self.xmax)))

def add_x_points_to_make_linterp_match_spline(x_points, spline, y_acc, xmin, xmax):
    i=0
    # an 'i' suffix means a value at X=x_points[i]
    # a  'j' suffix means a value at X=x_points[i+1]
    # a  'k' suffix means a value at X=half-way between
    xi = x_points[0]; yi = spline(xi)
    xj = x_points[1]; yj = spline(xj)
    while i+1 < len(x_points):
        xk = (xi+xj)/2
        yk = spline(xk) # where the spline says y(xk) should be
        ylint = (yi+yj)/2 # where linear interpolation says y(xk) should be
        if abs(ylint - yk) > y_acc:
            # too much difference - add a point at (xk, yk)
            x_points[i+1:i+1] = [xk]
            # leave i unchanged, so that we consider this interval for further subdivision
            xj = xk
            yj = yk
        else:
            # No need to add another point between (xi,yi) and (xj,yj): their midpoint is close enough to the spline.
            i += 1
            xi = xj
            yi = yj
            if i+1 >= len(x_points):
                break
            xj = x_points[i+1]; yj = spline(xj)

def make_spice_models(fho, ibis, modelname, modelname_out=None, typminmax_i=1, compname=None, ref_waveforms=False):
    time_scale = 1E9 # use ns as time units for V-t tables in Spice model
    dump_k_working = True
    
    if compname is None:
        compname = ibis.component_names
        if len(compname) > 1:
            raise ValueError('More than one component in this ibis file, you need to specify which to use')
        compname = compname[0]

    # typminmax_i = 1 for typ, 2 for min, 3 for max
    typminmax = (None, 'typical', 'minimum', 'maximum')[typminmax_i]
    if modelname_out is None:
        modelname_out = modelname.replace('/', '') + '_' + typminmax[:3].upper()
    print("Writing Spice model for %s strength %d, as '%s'" % (modelname, typminmax_i, modelname_out))
    
    comp = ibis.get_component_by_name(compname)
    model = ibis.get_model_by_name(modelname)

    model_type = model.model_type.lower()
    # Legal values for Model_type (per IBIS 6.1) are: 
    #  Input, Output, I/O, 3-state, Open_drain, I/O_open_drain, Open_sink, I/O_open_sink, 
    #  Open_source, I/O_open_source, Input_ECL, Output_ECL, I/O_ECL, 3-state_ECL, Terminator, 
    #  Series, and Series_switch. 
    if model_type == 'input':
        has_input = True
        has_output = False
    elif model_type == 'output':
        has_input = False
        has_output = True
        has_oe = False
    elif model_type == '3-state':
        has_input = False
        has_output = True
        has_oe = True
    elif model_type == 'i/o':
        has_input = True
        has_output = True
        has_oe = True
    else:
        # TODO: set up an exception type for this
        raise ValueError("Model type '"+model_type+"' is not currently supported by this tool")

    if not has_output:
        has_oe = False

    vcc = float(getattr(model.voltage_range, typminmax))
    c_comp = float(getattr(model.c_comp, typminmax))
    ref_tmax = None

    print("\n* =======================================================", file=fho)
    print("* Models for "+model.model_type+" "+modelname+" of component "+compname, file=fho)
    print("* =======================================================", file=fho)
    
    n_wf = min(len(getattr(model,'rising_waveforms')), len(getattr(model,'falling_waveforms')))
    if ref_waveforms and n_wf>0:
        # TODO: check len(getattr(model,'rising_waveforms'))==len(getattr(model,'falling_waveforms'))
        print("* -------------------------------------------------------", file=fho)
        # TODO: make number of outputs of this SUBCKT match the number of waveforms
        print(".SUBCKT "+modelname_out+"_REFWF TXD", file=fho, end='')
        for i in range(n_wf):
            print(' REF' + str(i+1), file=fho, end='')
        print("\n* Reference die-level "+model_type+" waveforms (without package parasitics)", file=fho)
        ref_tmax = None
        # TODO: handle waveforms not starting at t=0
        for i in range(n_wf):
            wf_r = getattr(model,'rising_waveforms')[i]
            wf_f = getattr(model,'falling_waveforms')[i]
            v_fixture = getattr(wf_r.v_fixture, typminmax)
            if v_fixture is None:
                v_fixture = getattr(wf_r.v_fixture, 'typical')
            print("* REF%d output is waveform when driving %s Ohm load into %s V" % (i+1, wf_r.r_fixture.to_eng_string(), v_fixture.to_eng_string() ), file=fho)
            # TODO: check that fixtures are the same for rising and falling waveforms
            if ref_tmax is None:
                ref_tmax = max(wf_r.table.samples[-1][0], wf_f.table.samples[-1][0])
            else:
                ref_tmax = max(ref_tmax, wf_r.table.samples[-1][0], wf_f.table.samples[-1][0])
        print("* max time in waveforms is " + str(ref_tmax) + ' s (corresponds to input=1.0 in the tables below)', file=fho)

        for section_name in ('rising_waveforms', 'falling_waveforms'):
            vtw = getattr(model, section_name)
            for i, wf in enumerate(vtw):
                wfname = str(i+1) + section_name[0:1].upper()
                print("E%s WF%s 0 T_%s 0 table=(" % (wfname, wfname, section_name[0:1].upper()), file=fho)
                for row in wf.table.samples:
                    print('+  ' + str(row[0]/ref_tmax) + ',' + row[typminmax_i].to_eng_string(), file=fho)
                print("+ )", file=fho)

        print('* Input logic -> drive high/low enables', file=fho)
        print('B1 ENPU 0 V=buf(V(TXD))', file=fho)
        print('B2 ENPD 0 V=inv(V(TXD))', file=fho)
                
        print('* Timebase generators (linear, changing to asymptotic approach when within 0.1% of final value)', file=fho)
        print('B3 0 T_R I=IF(ABS(V(ENPU)-V(T_R)) > 0.001, SGN(V(ENPU)-V(T_R)), 1000*(V(ENPU)-V(T_R)))', file=fho)
        print('C1 T_R 0 ' + str(ref_tmax), file=fho)
        print('RC1 0 T_R 10MEG', file=fho) # seems to be needed to prevent warnings
        print('B4 0 T_F I=IF(ABS(V(ENPD)-V(T_F)) > 0.001, SGN(V(ENPD)-V(T_F)), 1000*(V(ENPD)-V(T_F)))', file=fho)
        print('C2 T_F 0 ' + str(ref_tmax), file=fho)
        print('RC2 0 T_F 10MEG', file=fho) # seems to be needed to prevent warnings

        print('* Feed timebases into waveform tables, pick out the correct outputs', file=fho)
        for i in range(n_wf):
            print('B%d REF%d 0 V=IF(V(ENPU),V(WF%dR),V(WF%dF))' % (i+5, i+1, i+1, i+1), file=fho)
        print(".ENDS", file=fho)
       
    print("* -------------------------------------------------------", file=fho)
    print("* Main "+model_type+" buffer model (die model plus package parasitics)", file=fho)
    print(".SUBCKT "+modelname_out+" PKGPAD VSS VDD VDDNOM", end='', file=fho)
    if has_input:
        print(' RXD', end='', file=fho)
    if has_output:
        print(' TXD', end='', file=fho)
    if has_oe:
        print(' TXEN', end='', file=fho)
        
    print("\nVNOMSUPPLY VDDNOM VSS %e" % vcc, file=fho)

    if has_output:
        print("XPP   DIEPAD VSS VDD TXD "+('TXEN ' if has_oe else '') + modelname_out+"_DIE", file=fho)
    else:
        print("CCOMP VSS DIEPAD  %e" % c_comp, file=fho)        

    if has_input:
        print('BRX RXD 0 V=IF(V(DIEPAD,VSS) > %s, 1, IF(V(DIEPAD,VSS) < %s, 0, 0.5))' % (model.vinh.to_eng_string(), model.vinl.to_eng_string()), file=fho)

    print("XCD   DIEPAD VSS VDD  "+modelname_out+"_CLAMP", file=fho)
    print("LPKG  DIEPAD LRJ    " + getattr(comp.package.l_pkg, typminmax).to_eng_string(), file=fho)
    print("RPKG  LRJ    PKGPAD " + getattr(comp.package.r_pkg, typminmax).to_eng_string(), file=fho)
    print("CPKG  VSS    PKGPAD " + getattr(comp.package.c_pkg, typminmax).to_eng_string(), file=fho)
    print(".ENDS", file=fho)

    print("* -------------------------------------------------------", file=fho)
    if True: # TODO: omit this model if no clamp diodes are present?
        print("* Clamp diodes", file=fho)
        print(".SUBCKT "+modelname_out+"_CLAMP DIEPAD VSS VDD", file=fho)
        
        if model.gnd_clamp is not None:
            print("G1 DIEPAD VSS DIEPAD VSS table=(", file=fho)
            for row in model.gnd_clamp:
                print('+  ' + row[0].to_eng_string() + ',' + row[typminmax_i].to_eng_string(), file=fho)
            print("+ )", file=fho)

        if model.power_clamp is not None:
            print("G2 VDD DIEPAD VDD DIEPAD table=(", file=fho)
            for row in model.power_clamp:
                print('+  ' + row[0].to_eng_string() + ',' + row[typminmax_i].to_eng_string(), file=fho)
            print("+ )", file=fho)
        
        print(".ENDS", file=fho)

    if not has_output:
        return

    print("* -------------------------------------------------------", file=fho)
    print("* Die-level "+model_type+" buffer model (without package parasitics)", file=fho)
    print("* Use this model for comparison with reference waveforms, but not for real circuit simulations", file=fho)
    print("* If fed into the correct loads, these should give the same outputs as the reference waveforms.", file=fho)
    if has_oe:
        print(".SUBCKT "+modelname_out+"_DIE DIEPAD VSS VDD TXD TXEN", file=fho)
        print('* Input logic -> drive high/low enables', file=fho)
        print('B3 ENPU 0 V=IF(V(TXEN) &   V(TXD) , 1, 0)', file=fho)  # TODO are the IFs needed or does boolean logic generate 0/1 anyway?
        print('B4 ENPD 0 V=IF(V(TXEN) & (!V(TXD)), 1, 0)', file=fho)
    else:
        print(".SUBCKT "+modelname_out+"_DIE DIEPAD VSS VDD TXD", file=fho)
        print('* Input logic -> drive high/low enables', file=fho)
        print('B3 ENPU 0 V=buf(V(TXD))', file=fho)
        print('B4 ENPD 0 V=inv(V(TXD))', file=fho)
    # http://ltwiki.org/?title=B_sources_(complete_reference)
    # http://ltwiki.org/index.php?title=B_sources_(common_examples)
    # B111 st 0 V=sdt(1,0,buf(abs(ddt(V(state))))) ; node "st" is "state time"
    # B222 blah 0 V=table(st, x1, y1, x2, y2, ...)
    # B1 ENPU_T 0 V=idt(VT_DUR*sgn(V(ENPU)-V(ENPU_T)))
    
    # fetch the appropriate I-V curves, convert from Decimal to float
    vi_pd = tuple((float(r[0]), float(r[typminmax_i])) for r in model.pulldown)
    vi_pu = tuple((float(r[0]), float(r[typminmax_i])) for r in model.pullup)

    t_min = None
    for section_name in ('rising_waveforms', 'falling_waveforms'):
        # select two V-t tables to use
        vtw = getattr(model, section_name)
        if len(vtw) < 2:
            # TODO: support conversion to spice with only one V-t table
            raise RuntimeError("At least two " + section_name + " tables are needed")
        if len(vtw) > 2:
            print("The first two two " + section_name + " tables will be used, others will be ignored")

        # TODO: use scipy.interpolate.interp1d to add extra interpolated datapoints in areas where V changes rapidly?

        # get the fixture resistance and voltage for each table we are using
        r_fixture = [float(vtw[lci].r_fixture) for lci in (0,1)]
        v_fixture = []
        for lci in (0,1):
            v_f = getattr(vtw[lci].v_fixture, typminmax)
            if v_f is None:
                v_f = getattr(vtw[lci].v_fixture, 'typical')
            v_fixture.append(float(v_f))
            print("Using %s table with Vcc=%.3f, V_fixture=%.3f, R_fixture=%.1f" % (section_name, vcc, v_fixture[lci], r_fixture[lci]))

        # calculate the expected output voltage given the fixture load resistance/voltage, for each end of each V-t table
        # TODO: why is this not matching?
        v_loaded = [[None, None], [None, None]]
        for lci in (0,1):
            for logicstate in (0,1):
                iv = [] # column 0 = current, column 1 = voltage
                if logicstate:
                    # driving high: pulldown transistor is off, pullup transistor is on
                    #~ print "  driving high into load pulling to V_fixture=$v_fixture[$lci]:";
                    for j in range(len(vi_pu)):
                        iv.append( [vi_pu[j][1], vcc-vi_pu[j][0]] )
                else:
                    # driving low: pulldown transistor is on, pullup transistor is off
                    #~ print "  driving low into load pulling to V_fixture=$v_fixture[$lci]:";
                    for j in range(len(vi_pd)):
                        iv.append( [vi_pd[j][1], vi_pd[j][0]] )

                for j in range(len(iv)):
                    #~ printf " \tI=%.3f, V=%.3f", @{$iv[$j]};
                    iv[j][0] -= (v_fixture[lci] - iv[j][1])/r_fixture[lci]
                    #~ printf " \tI=%.3f, V=%.3f\n", @{$iv[$j]};
                v_loaded[lci][logicstate] = Spline(x=tuple(float(r[0]) for r in iv), y=tuple(float(r[1]) for r in iv), copy=False, kind='linear')(0)
                print(" Vout=%.3f" % v_loaded[lci][logicstate])

        # extract the V-t curves we are interested in
        vt_t = [ [time_scale*float(r[0]) for r in vtw[lci].table.samples] for lci in (0,1) ]
        vt_v = [ [float(r[typminmax_i])  for r in vtw[lci].table.samples] for lci in (0,1) ]
        # make V-t splines
        vtspline = [ Spline(x=vt_t[lci], y=vt_v[lci], kind='cubic') for lci in (0,1) ]

        #~ plt.plot(vt_t[0], vt_v[0], color="green")
        #~ plt.plot(vt_t[1], vt_v[1], color="blue")

        # work out a common time range and timestep to use
        t_min = min(vt_t[0][ 0], vt_t[1][ 0])
        t_max = max(vt_t[0][-1], vt_t[1][-1])
        t_step = (t_max - t_min)/20

        # select time-points to use
        tns = [ t_min + t_step * i for i in range(int((t_max-t_min)/t_step)) ]
        tns.append(t_max)
        #~ print('%d time points covering %.3f to %.3f ns' % (len(tns), tns[0], tns[-1]))
        for lci in (0,1):
            add_x_points_to_make_linterp_match_spline(tns, vtspline[lci], vcc/1000, vt_t[lci][0], vt_t[lci][-1])
        print('%d time points covering %.3f to %.3f ns' % (len(tns), tns[0], tns[-1]))
        t_dur = tns[-1] - tns[0]
        dtd = 0.001/time_scale # dtd is the delta-t (in seconds) to use when calculating derivatives TODO: choose a value for this based on the waveform

        #~ plt.plot(tns, tuple(vtspline[0](t) for t in tns), color="red")
        #~ plt.plot(tns, tuple(vtspline[1](t) for t in tns), color="red")
        #~ plt.show()

        # compare the V-I voltage to the V-t voltage
        for lci in (0,1):
            for i in (0,-1): # look at beginning and end of V-t table
                logicstate = (section_name == 'falling_waveforms') ^ (i == -1)
                print("  driving high" if logicstate else "  driving low", end='')
                print(" into %.1f Ohm load pulling to %.3f V:" % (r_fixture[lci], v_fixture[lci]), end='')
                v_vi = v_loaded[i][logicstate]
                v_vt = vt_v[lci][i]
                print(" Vout (from I-V tables)=%.3f, Vout (from V-t tables)=%.3f, difference = %d%% of Vcc" % (float(v_vi), float(v_vt), 100*(v_vt-v_vi)/float(vcc)))

        # make K(t) tables
        vi_pu_spline = Spline(x=tuple(float(r[0]) for r in vi_pu), y=tuple(float(r[1]) for r in vi_pu), copy=False, kind='linear')
        vi_pd_spline = Spline(x=tuple(float(r[0]) for r in vi_pd), y=tuple(float(r[1]) for r in vi_pd), copy=False, kind='linear')
        
        if model.gnd_clamp is not None:
            gnd_clamp_spline = Spline(x=tuple(float(r[0]) for r in model.gnd_clamp), y=tuple(float(r[typminmax_i]) for r in model.gnd_clamp), copy=False, kind='linear')
        else:
            gnd_clamp_spline = lambda v: 0.0
        
        if model.power_clamp is not None:
            power_clamp_spline = Spline(x=tuple(float(r[0]) for r in model.power_clamp), y=tuple(float(r[typminmax_i]) for r in model.power_clamp), copy=False, kind='linear')
        else:
            power_clamp_spline = lambda v: 0.0
            
        ku = []
        kd = []
        if dump_k_working:
            print("* ==== " + section_name + " K(t) calculations ====", file=fho)
        for t in tns:
            # Our model is that the output current is a function of time t and output voltage Vo, of the form
            # -Iout(t) = Ku(t).Ipu_on(Vcc-Vo)   : current through the pull-up transistor
            #          + Kd(t).Ipd_on(Vo)       : current through the pull-down transistor
            #          + Ipc(Vcc-Vo)            : current through the power-rail clamp diode
            #          + Igc(Vo)                : current through the ground clamp diode
            #          + Ccomp.dV/dt           : current through the capacitor
            # The Ipu_on(V), Ipd_on(V), Ipc(V), Igc(V) curves and Ccomp are given to us in the IBIS file
            # We can get Iout(t) from Vout(t) for each of the given resistive load conditions (fixtures) using Ohm's law
            # We need to deduce Ku(t) and Kd(t) from V(t) curves given for two different load conditions.
            
            # Get the Vout(t) value for the two load conditions
            v = [vtspline[lci](t) for lci in (0,1)]
            # Calculate the capacitor current for the two load conditions
            cap_current = [ c_comp*((vtspline[lci](t + time_scale*dtd) - v[lci] )/dtd) for lci in (0,1) ] # I=C.dV/dt
            
            for lci in (0,1):
                try:
                    power_clamp_spline(v[lci]-vcc)
                except ValueError:
                    raise ValueError('Could not evaluate power clamp at %.6f V' % (v[lci]-vcc,))

            # Calculate the transistor current at time t
            current = [ 
                (v_fixture[lci] - v[lci])/r_fixture[lci]  # the current through the die pad
                - cap_current[lci]                        # minus the capacitor current
                #- power_clamp_spline(vcc-v[lci])          # and the power-clamp current
                #- gnd_clamp_spline(v[lci])                # and the ground-clamp current
                for lci in (0,1) ]                        # for both load conditions
            # Look up how much current the transistors would be passing if they were fully on
            ipu = [ vi_pu_spline(vcc-v[lci]) for lci in (0,1) ] # Ipu_on(Vcc-Vo)
            ipd = [ vi_pd_spline(    v[lci]) for lci in (0,1) ] # Ipd_on(Vo) 
            # we want to find ku and kd such that kd*ipd[0] + ku*ipu[0], kd*ipd[1] + ku*ipu[1]: solve this by matrix inversion
            (a, b, c, d) = (
                ipu[0], ipd[0],
                ipu[1], ipd[1],
                )
            rdet = a*d - b*c
            # solve for ku and kd
            ku.append((d*current[0] - b*current[1])/rdet)
            kd.append((a*current[1] - c*current[0])/rdet)
            if dump_k_working:
                print("* t=%.3fns" % t, end='', file=fho)
                print(" \tV=%.6f,%.6f" % tuple(v), end='', file=fho)
                print(" \tIc=%.3f,%.3f" % tuple(cap_current), end='', file=fho)
                print(" \tI=%.3f,%.3f" % tuple(current), end='', file=fho)
                print(" \tIpu=%.6f,%.6f" % tuple(ipu), end='', file=fho)
                print(" \tIpd=%.6f,%.6f" % tuple(ipd), end='', file=fho)
                #~ print(" \tIpc=%.6f,%.6f" % (power_clamp_spline(vcc-v[0]), power_clamp_spline(vcc-v[1])), end='', file=fho)
                print(" \tIgc=%.6f,%.6f" % (gnd_clamp_spline(v[0]), gnd_clamp_spline(v[1])), end='', file=fho)
                print(" \trdet=%.6f" % rdet, end='', file=fho)
                print(" \tKu=%.3f,Kd=%.3f" % (ku[-1], kd[-1]), end='', file=fho)
                print(" \tI=%.3f,%.3f" % ( kd[-1]*ipd[0] + ku[-1]*ipu[0], kd[-1]*ipd[1] + ku[-1]*ipu[1] ), file=fho)

        # output the K(t) tables
        for driver in ('u', 'd'):
            print('.SUBCKT LUT_K' + driver.upper()+ '_' + section_name[:1].upper() + "E 1 2 3 4", file=fho)
            print("E1 1 2 3 4 table = (", file=fho)
            for i in range(len(tns)):
                print("+  %.4f,%.4f" % (
                    (tns[i] - t_min)/t_dur,
                    (ku[i] if driver=='u' else kd[i]),
                    ), file=fho)
            print("+ )\n.ENDS", file=fho)
    
    for pull in ('pullup', 'pulldown'):
        print("\n* " + pull + " transistor I-V table", file=fho)
        print('.SUBCKT ' + pull.upper() + " 1 2 3 4", file=fho)
        print("E1 1 2 3 4 table = (", file=fho)
        for r in getattr(model, pull):
            print("+  %.4f,%.4f" % (r[0], r[typminmax_i]), file=fho)
        print("+ )\n.ENDS", file=fho)
    for line in (
        '* Transition slopes (linear, changing to asymptotic approach when within 0.1% of final value)',
        'B1 0 ENPU_T I=IF(ABS(V(ENPU)-V(ENPU_T)) > 0.001, SGN(V(ENPU)-V(ENPU_T)), 1000*(V(ENPU)-V(ENPU_T)))',
        'C1 ENPU_T 0 ' + str(t_dur/time_scale),
        'RC1 0 ENPU_T 10MEG', # seems to be needed to prevent warnings
        # Same type of slope generator for the pull-down transistor
        'B2 0 ENPD_T I=IF(ABS(V(ENPD)-V(ENPD_T)) > 0.001, SGN(V(ENPD)-V(ENPD_T)), 1000*(V(ENPD)-V(ENPD_T)))',
        'C2 ENPD_T 0 ' + str(t_dur/time_scale),
        'RC2 0 ENPD_T 10MEG', # seems to be needed to prevent warnings
        
        '* V-I look-up tables for output transistors',
        'XE1  PD_CURR 0  DIEPAD VSS  PULLDOWN',
        'XE2  PU_CURR 0  VDD DIEPAD  PULLUP',
        '* V-t look-up tables for on-ness of output transistors',
        'V1 C1V 0 1.0',
        'XE5  KPU_RE 0  ENPU_T   0  LUT_KU_RE',
        'XE6  KPU_FE 0  C1V ENPU_T  LUT_KU_FE',
        'XE8  KPD_FE 0  ENPD_T   0  LUT_KD_FE',
        'XE7  KPD_RE 0  C1V ENPD_T  LUT_KD_RE',
        '* The output current driver',
        'B5 DIEPAD 0 I=V(PU_CURR)*IF(V(ENPU),V(KPU_RE),V(KPU_FE)) + V(PD_CURR)*IF(V(ENPD),V(KPD_FE),V(KPD_RE))',
        # '* Debug output',
        # 'EDBG  DBG 0  C1V ENPD_T  1',
    ):
        print(line, file=fho)

    print("XCD   DIEPAD VSS VDD  "+modelname_out+"_CLAMP", file=fho)
    print("CCOMP VSS DIEPAD %e" % c_comp, file=fho)
    
    print(".ENDS", file=fho)      

    return {'modelname': modelname_out, 'tmax': ref_tmax}

def format_nullable(fmt, val):
    if val is None:
        return 'None'
    else:
        return fmt % val

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Specify an ibis file name and one or more model names, or just an ibis file to list the models')
        exit()

    ibs_file = ecdtools.ibis.load_file(sys.argv[1], transform=True)

    if len(sys.argv) < 3:
        print('Component names:')
        for name in ibs_file.component_names:
            print('    ' + name)

        print('Model names:')
        for name in ibs_file.model_names:
            model = ibs_file.get_model_by_name(name)
            print('    %s (%s)' % (name, model.model_type), end='')
            for aname in ('gnd_clamp', 'power_clamp', 'pullup', 'pulldown', 'ramp'):
                if getattr(model, aname) is not None:
                    print(' ' + aname, end='')
            print()
            for aname in ('falling_waveforms', 'rising_waveforms'):
                for waveform in getattr(model, aname):
                    print('        %s R=%s V=%s/%s/%s' % (aname[:-1], 
                        format_nullable('%.1f', waveform.r_fixture), 
                        format_nullable('%.2f', waveform.v_fixture.typical), 
                        format_nullable('%.2f', waveform.v_fixture.minimum), 
                        format_nullable('%.2f', waveform.v_fixture.maximum)
                        ))
        exit()

    with open('out.mod', 'w') as fho:
        for model_name in sys.argv[2:]:
            make_spice_models(fho, ibs_file, model_name, typminmax_i=1, ref_waveforms=True)
