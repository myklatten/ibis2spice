#!/usr/bin/perl

# extract all the t-V and V-I tables from an IBIS file and save them seperately

use strict;
use warnings;

#~ split_ibis('lm3s9b90_lqfp_v0p1.ibs','lm3s9b90_lqfp_v0p1');
split_ibis('xc3sa.ibs','spartan3a', qr/\A LVCMOS33_F .* _33 \z/xms);

sub split_ibis {
    my ($infile, $outdir, $model_match) = @_;

    my $fhi;
    (-f $infile && -r $infile) || die "'$infile' is not readable";
    open $fhi, '<', $infile || die "Could not open '$infile' for reading: $!";
    
    my $model_name = q{};
    my $section_name = q{};
    my $section_contents = q{};
    
    while (defined (my $line = <$fhi>)) {
        chomp $line;
        if ($line =~ /\A \s* \[ (.*?) \] \s* (.*?) \z/xmsi) {
            my ($in_brackets, $after_brackets) = ($1, $2);
            process_section($model_name, $section_name, $section_contents, $outdir) if (!defined $model_match) || ($model_name =~ $model_match);
            $section_name = lc $in_brackets;
            $model_name = $after_brackets if $section_name eq 'model';
            $section_contents = q{};
        }
        else {
            $section_contents .= $line . "\n";
        }
    }
}

sub process_section {
    my ($model_name, $section_name, $section_contents, $outdir) = @_;
    #~ print "model '$model_name', section '$section_name': " . length($section_contents) . " bytes\n";
    return unless $model_name ne q{} && $section_name =~ /\A 
        (
          falling [ ] waveform
        | rising [ ] waveform
        | pulldown 
        | pullup 
        #| gnd [ ] clamp 
        #| power [ ] clamp 
    ) \z/xms;
    
    my $outfile = "$model_name $section_name";
    
    $section_contents = "\n" . $section_contents;
    if ($section_contents =~ / \v V_fixture \s* = \s* (\S*) \v /xmsi) {
        $outfile .= ' ' . $1;
    }
    substr $section_contents,0,1,q{}; # remove the initial "\n" we just added
    
    $outfile =~ tr{/\\}{__};
    $outfile = "$outdir/$outfile.txt";
    print "save to $outfile\n";
    #~ return;
    
    my $fho;
    open $fho, '>', $outfile || die "Could not open '$outfile' for writing: $!";
    print {$fho} $section_contents;
    close $fho;

}