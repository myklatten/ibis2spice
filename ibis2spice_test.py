import ecdtools
from ibis2spice import make_spice_models
import subprocess
import os
import os.path
import re
from math import sqrt

# TODO: get the test fixture data from the IBIS model
# TODO: get the .tran duration from the model V-t table size

ltspice_path = None
for pospath in (
    r'C:\Program Files\LTC\LTspiceXVII\XVIIx64.exe',
    r'D:\Program Files\LTC\LTspiceXVII\XVIIx64.exe',
):
    if os.path.isfile(pospath):
        ltspice_path = pospath
        break
if ltspice_path is None:
    raise RuntimeError('Cannot find LTSpice executable')

if not os.path.isdir('test_workspace'):
    os.makedirs('test_workspace')

testcases = [
    ['tm4c1294_short.ibs', 'GPIO8P_I/O_8'],
    # ['sample1.ibs', 'BPOZ2F', 'BPOZ4F', 'BPS2P10F_PU50K'],
]

for testcase in testcases:
    ibs_file = ecdtools.ibis.load_file(testcase[0], transform=True)
    assert ibs_file is not None

    modelnames_out = []
    with open('test_workspace/out.mod', 'w') as fho:
        for model_name in testcase[1:]:
            modelnames_out.append( make_spice_models(fho, ibs_file, model_name, typminmax_i=1, ref_waveforms=True))['modelname']

    for model_name in modelnames_out:
        nfix = 2
        with open('test_workspace/test_edges.cir', 'w') as fho:
            # title
            print('* test_edges.cir', file=fho)
            # power rail
            print('V1 VDD 0 3.3', file=fho)
            # driver control
            print('V2 TXD 0 PULSE(0 1 5n .1n .1n 25n 50n)', file=fho)
            # ref waveform generator
            print('XU3 TXD out_ref_f1 out_ref_f2 %s_REFWF' % model_name, file=fho)
            # die model with test fixture 1
            print('XU1 out_f1 0 VDD TXD VDD %s_DIE' % model_name, file=fho)
            print('R1 VDD out_f1 50', file=fho)
            # die model with test fixture 2
            print('XU2 out_f2 0 VDD TXD VDD %s_DIE' % model_name, file=fho)
            print('R2 out_f2 0 50', file=fho)
            #* simulation control
            print('.tran 36n', file=fho)
            print('.option noopiter', file=fho)
            print('.lib out.mod', file=fho)
            print('.backanno', file=fho)
            for i in range(1, nfix + 1):
                # measure model quality with test fixture i
                print('.meas tran maxdiff%d MAX V(out_f%d,out_ref_f%d)' % (i,i,i), file=fho)
                print('.meas tran mindiff%d MIN V(out_f%d,out_ref_f%d)' % (i,i,i), file=fho)
                print('.meas tran rmsdiff%d RMS V(out_f%d,out_ref_f%d)' % (i,i,i), file=fho)
                print('.meas tran areadiff%d INTEG abs(V(out_f%d,out_ref_f%d))' % (i,i,i), file=fho)
            print('.end', file=fho)

        cp = subprocess.run([ltspice_path, '-b', r'test_edges.cir'], cwd='test_workspace')
        assert cp.returncode == 0

        # maxdiff1: MAX(v(out_f1,out_ref_f1))=0.0103872 FROM 0 TO 3.6e-008
        # mindiff1: MIN(v(out_f1,out_ref_f1))=-0.011219 FROM 0 TO 3.6e-008
        # rmsdiff1: RMS(v(out_f1,out_ref_f1))=0.000961354 FROM 0 TO 3.6e-008
        # areadiff1: INTEG(abs(v(out_f1,out_ref_f1)))=1.0908e-011 FROM 0 TO 3.6e-008

        meas = {
            'maxdiff':  list(None for i in range(nfix+1)),
            'mindiff':  list(None for i in range(nfix+1)),
            'rmsdiff':  list(None for i in range(nfix+1)),
            'areadiff': list(None for i in range(nfix+1)),
        }

        with open('test_workspace/test_edges.log', 'r') as fhi:
            for line in fhi:
                m = re.match(r'(\w+)(\d+): (\S*?)=([+-]?[0-9]+\.?[0-9]*([Ee][+-]?[0-9]+)?) FROM \S+ TO ', line)
                if m:
                    mtype, ifix, val = m.group(1), int(m.group(2)), float(m.group(4))
                    if (mtype in meas) and (ifix > 0) and (ifix <= nfix):
                        meas[mtype][ifix] = val
                    else:
                        print("Unexpected measurement '%s%d'" % (mtype, ifix))
        meas['maxdiff'][0] = max(meas['maxdiff'][1:])
        meas['mindiff'][0] = min(meas['mindiff'][1:])
        meas['rmsdiff'][0] = sqrt(sum(v*v for v in meas['rmsdiff'][1:]))
        meas['areadiff'][0] = sum(meas['areadiff'][1:])
        print(meas)
        print('Extreme errors: %.1f - %.1f mV; integrated error: %.1f ns-mV' % (meas['mindiff'][0]*1000, meas['maxdiff'][0]*1000, meas['areadiff'][0]*1E12))