#!/usr/bin/perl

# extract all the info from an IBIS file

#~ use YAML;
use Math::Spline;

use strict;
use warnings;


my %si_scaler = (
    meg => 1E6,
    k => 1E3,
    q{} => 1,
    m => 1E-3,
    u => 1E-6,
    n => 1E-9,
    p => 1E-12,
    f => 1E-15,
);

my %section_type = (
    # how to handle stuff on the same line as the [section] header, and how to handle stuff on following lines
    'ibis_ver'   => ['text', undef],
    'file_name'  => ['text', undef],
    'file_rev'   => ['text', undef],
    'date'       => ['text', undef],
    'comment_char' => ['text', undef],
    'source'     => ['text', 'text'],
    'notes'      => ['text', 'text'],
    'disclaimer' => ['text', 'text'],
    'copyright'  => ['text', 'text'],
    'component'    => ['text', undef],
    'manufacturer' => ['text', undef],
    'package'    => [undef, 'k_mv'],
    'pin'        => ['table_headings', 'k_mv'], # each line has a key with multiple values, and section line provides names for these values
    'diff_pin'   => ['table_headings', 'k_mv'],
    
    'model'             => ['text', undef],
    'model_parameters'  => [undef, 'k_mv'],
    'model_spec'        => [undef, 'k_mv'],
    'temperature_range' => ['mv', undef], # multiple values, no key specified in file
    'voltage_range'     => ['mv', undef],
    'power_clamp_reference' => ['mv', undef],
    'gnd_clamp_reference'   => ['mv', undef],
    'ramp'              => [undef, 'k_mv'], # each line has a key with multiple values, or "key = single_value"
    'pulldown'          => [undef, 'table'],
    'pullup'            => [undef, 'table'],
    'gnd_clamp'         => [undef, 'table'],
    'power_clamp'       => [undef, 'table'],
    'rising_waveform'   => [undef, 'table'],
    'falling_waveform'  => [undef, 'table'],
);

#~ my $vcc = desuffix_number('3.60V'); print "$vcc\n"; exit 0;

#~ my $ibis = read_ibis('D:/t/spice/82375sb.ibs',undef, qr/\A PCEBB12211A0S2AZZZZC \z/xms);
#~ print Dump($ibis); exit 0;
#~ print Dump($ibis->{model}{LVCMOS33_F_4_LR_33}{'rising_waveform'});

if (0) {
    my $ibis = read_ibis('R:/Hardware/Product Design data/PCB Designs/EtherDIO/90ED593R2/Documents/Protection/TPD2E2U06.ibs',undef, undef);
    ibis_make_tables_numeric($ibis);

    #~ save_vt_tables($ibis, 'LVCMOS33_F_4_LR_33', 1, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/xc3sa_vt_'    , 2.05E-9, 17.55E-9);
    #~ save_vt_tables($ibis, 'LVCMOS33_F_4_LR_33', 2, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/xc3sa_vt_min_', 2.05E-9, 17.55E-9);
    #~ save_vt_tables($ibis, 'LVCMOS33_F_4_LR_33', 3, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/xc3sa_vt_max_', 2.05E-9, 17.55E-9);

    my $outfile = 'R:/Hardware/Product Design data/PCB Designs/EtherDIO/90ED593R2/Documents/Protection/TPD2E2U06.lib';
    my $fho;
    open $fho, '>', $outfile or die "Could not open '$outfile' for writing: $!";
    make_spice_models($fho, $ibis, 'IN_50A', 'IN_50A', 1);
    close $fho;
}

if (0) {
    my $ibis = read_ibis('xc3sa_ft256.ibs',undef, qr/\A LVCMOS33_F_[48]_LR_33 \z/xms);
    ibis_make_tables_numeric($ibis);

    save_vt_tables($ibis, 'LVCMOS33_F_4_LR_33', 1, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/xc3sa_vt_'    , 2.05E-9, 17.55E-9);
    save_vt_tables($ibis, 'LVCMOS33_F_4_LR_33', 2, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/xc3sa_vt_min_', 2.05E-9, 17.55E-9);
    save_vt_tables($ibis, 'LVCMOS33_F_4_LR_33', 3, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/xc3sa_vt_max_', 2.05E-9, 17.55E-9);

    my $outfile = 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/xs3a.lib';
    my $fho;
    open $fho, '>', $outfile or die "Could not open '$outfile' for writing: $!";
    for my $ds (4, 8) {
        make_spice_models($fho, $ibis, "LVCMOS33_F_${ds}_LR_33", "C33F${ds}"    , 1);
        make_spice_models($fho, $ibis, "LVCMOS33_F_${ds}_LR_33", "C33F${ds}_MIN", 2);
        make_spice_models($fho, $ibis, "LVCMOS33_F_${ds}_LR_33", "C33F${ds}_MAX", 3);
    }
    close $fho;
}

if (1) {
    my $ibis = read_ibis('tm4c1294_short.ibs',undef, qr{\A I/O_[48] \z}xms);
    ibis_make_tables_numeric($ibis);
    # GPIO8P_I/O_8
    my $outfile = 'tm4c1294_perl.lib';
    my $fho;
    open $fho, '>', $outfile or die "Could not open '$outfile' for writing: $!";
    make_spice_models($fho, $ibis, 'GPIO8P_I/O_8', 'GPIO8PIO8', 1);
    close $fho;
}

if (0) {
    my $ibis = read_ibis('lm3s9b90_lqfp_v0p1.ibs',undef, qr{\A I/O_[48] \z}xms);
    ibis_make_tables_numeric($ibis);
    
    save_vt_tables($ibis, 'I/O_4', 1, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/uc_vt_'    , 2.05E-9, 28.05E-9);
    save_vt_tables($ibis, 'I/O_4', 2, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/uc_vt_min_', 2.05E-9, 28.05E-9);
    save_vt_tables($ibis, 'I/O_4', 3, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/uc_vt_max_', 2.05E-9, 28.05E-9);

    my $outfile = 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/lm3s9b90.lib';
    my $fho;
    open $fho, '>', $outfile or die "Could not open '$outfile' for writing: $!";
    for my $ds (4, 8) {
        make_spice_models($fho, $ibis, "I/O_${ds}", "IO${ds}"    , 1);
        make_spice_models($fho, $ibis, "I/O_${ds}", "IO${ds}_MIN", 2);
        make_spice_models($fho, $ibis, "I/O_${ds}", "IO${ds}_MAX", 3);
    }
    close $fho;
}

sub save_vt_tables {
    my ($ibis, $model, $typminmax_i, $outfile_base, $toff_re, $toff_fe) = @_;
    
    for my $ti (0 .. $#{$ibis->{model}{$model}{'rising_waveform'}}) {
        
        my $outfile = $outfile_base . $ti . '.txt';
        my $fho;
        open $fho, '>', $outfile or die "Could not open '$outfile' for writing: $!";
        
        for my $section_name ('rising_waveform', 'falling_waveform') {
            my $table = $ibis->{model}{$model}{$section_name}[$ti]{table};
            my $toff = $section_name eq 'rising_waveform' ? $toff_re : $toff_fe;
            
            # create a spline
            my $vtspline = Math::Spline->new( 
                [ map { 1E9 * $_->[0]      } @{$table} ], # X is time in ns
                [ map { $_->[$typminmax_i] } @{$table} ] # Y is voltage in V
            ); 
            # select time-points to use
            my @tns = map { 1E9 * $_->[0] } @{$table}; # take time points from table, convert to ns
            for my $ti (0,1) {
                add_x_points_to_make_linterp_match_spline(\@tns, $vtspline, 3.3/1000);
            }
            
            #~ print "$section_name $ti\n"; binmode STDOUT;
            for my $x (@tns) {
                my $t = $toff + 1E-9 * $x;
                last if $section_name eq 'rising_waveform' && $t>=$toff_fe;
                print {$fho} "$t," . $vtspline->evaluate($x) . "\n";
            }
        }
        
        close $fho;
    }
    
}

sub make_spice_models {
    my ($fho, $ibis, $model, $modelname, $typminmax_i) = @_;
    $typminmax_i = 1 if !defined $typminmax_i; # 1 for typ, 2 for min, 3 for max
    $modelname = $model . '_' . $typminmax_i if !defined $modelname;
    print "Writing Spice model for $model strength $typminmax_i, as '$modelname'\n";
    
    my $vcc = desuffix_number($ibis->{model}{$model}{voltage_range}[$typminmax_i-1]);
    my $c_comp = $ibis->{model}{$model}{model_parameters}{c_comp}[$typminmax_i-1];
    
    print {$fho} "\n* =======================================================\n";
    print {$fho} "* Models for I/O ${model}\n";
    print {$fho} "* =======================================================\n";
    
    print {$fho} "* Input (clamp diodes, component capacitance and package parasitics)\n";
    print {$fho} ".SUBCKT ${modelname}_IN PIN DIEPAD VEE VCC\n";
    print {$fho} "XCD   DIEPAD VEE VCC  ${modelname}_CD\n";
    print {$fho} "CCOMP VEE DIEPAD  $c_comp\n";
    print {$fho} "LPKG  DIEPAD LRJ  $ibis->{package}{l_pkg}[$typminmax_i-1]\n";
    print {$fho} "RPKG  LRJ    PIN  $ibis->{package}{r_pkg}[$typminmax_i-1]\n";
    print {$fho} "CPKG  VEE    PIN  $ibis->{package}{c_pkg}[$typminmax_i-1]\n";
    print {$fho} ".ENDS\n";
    
    print {$fho} "* -------------------------------------------------------\n";
    print {$fho} "* Output (push-pull transistors, clamp diodes and package parasitics)\n";
    print {$fho} ".SUBCKT ${modelname}_OUT IN EN OUT VEE VCC\n";
    print {$fho} "XPP   IN EN DIEPAD VEE VCC  ${modelname}_PP\n";
    print {$fho} "XCD   DIEPAD VEE VCC  ${modelname}_CD\n";
    print {$fho} "LPKG  DIEPAD LRJ  $ibis->{package}{l_pkg}[$typminmax_i-1]\n";
    print {$fho} "RPKG  LRJ    OUT  $ibis->{package}{r_pkg}[$typminmax_i-1]\n";
    print {$fho} "CPKG  VEE    OUT  $ibis->{package}{c_pkg}[$typminmax_i-1]\n";
    print {$fho} ".ENDS\n";
    
    print {$fho} "* -------------------------------------------------------\n";
    {
        print {$fho} "* Clamp diodes\n";
        print {$fho} ".SUBCKT ${modelname}_CD DIEPAD VEE VCC\n";
        
        print {$fho} "G1 DIEPAD VEE DIEPAD VEE table=(\n";
        my $table = $ibis->{model}{$model}{gnd_clamp}[0]{table};
        for my $i (0..$#{$table}) {
            printf {$fho} "+  %s,%s\n",
                $table->[$i][0],
                $table->[$i][$typminmax_i],
                ;
        }
        print {$fho} "+ )\n";
        
        print {$fho} "G2 VCC DIEPAD VCC DIEPAD table=(\n";
        $table = $ibis->{model}{$model}{power_clamp}[0]{table};
        for my $i (0..$#{$table}) {
            printf {$fho} "+  %s,%s\n",
                $table->[$i][0],
                $table->[$i][$typminmax_i],
                ;
        }
        print {$fho} "+ )\n";
        
        print {$fho} ".ENDS\n";
    }
    
    print {$fho} "* -------------------------------------------------------\n";
    print {$fho} "* Push-pull transistors\n";
    print {$fho} ".SUBCKT ${modelname}_PP IN EN OUT VEE VCC\n";
    print {$fho} <<'ENDMODEL';
* Input logic -> drive high/low enabkes    
B3 ENPU 0 V=IF(V(EN) & V(IN), 1, 0)
B4 ENPD 0 V=IF(V(EN) & (!V(IN)), 1, 0)
* Transition slopes (linear)
B1 0 ENPU_T I=IF(ABS(V(ENPU)-V(ENPU_T)) > 0.001, SGN(V(ENPU)-V(ENPU_T)), 1000*(V(ENPU)-V(ENPU_T)))
C1 ENPU_T 0 {VT_DUR}
B2 0 ENPD_T I=IF(ABS(V(ENPD)-V(ENPD_T)) > 0.001, SGN(V(ENPD)-V(ENPD_T)), 1000*(V(ENPD)-V(ENPD_T)))
C2 ENPD_T 0 {VT_DUR}
* Look-up tables
V1 C1V 0 1.0
XE1  PD_CURR 0  OUT VEE  PULLDOWN
XE2  PU_CURR 0  VCC OUT  PULLUP
XE5  KPU_RE 0  ENPU_T   0  LUT_KU_RE
XE6  KPU_FE 0  C1V ENPU_T  LUT_KU_FE
XE8  KPD_FE 0  ENPD_T   0  LUT_KD_FE
XE7  KPD_RE 0  C1V ENPD_T  LUT_KD_RE
* The output current driver
B5 OUT 0 I=V(PU_CURR)*IF(V(ENPU),V(KPU_RE),V(KPU_FE)) + V(PD_CURR)*IF(V(ENPD),V(KPD_FE),V(KPD_RE))
* Debug output
*EDBG  DBG 0  C1V ENPD_T  1
ENDMODEL

    #~ * logic low:  ENPD=1, ENPU=0; ENPD_T->1, ENPU_T->0; KPD->1, KPU->0
    #~ * logic high: ENPD=0, ENPU=1; ENPD_T->0, ENPU_T->1; KPD->0, KPU->1
    #~ *B5 OUT 0 I=V(PU_CURR)*V(ENPU_T) + V(PD_CURR)*V(ENPD_T) ; gives correct I-V curves
    #~ *B5 OUT 0 I=IF(V(ENPU),V(PU_CURR),V(PD_CURR)) ; gives correct I-V curves
    #~ *B5 OUT 0 I=IF(V(ENPU), V(PU_CURR)*V(KPU_RE), V(PD_CURR)*V(KPD_FE))  ; gives correct I-V curves and driven edges
    #~ *B5 OUT 0 I=IF(V(ENPU), V(PU_CURR)*V(KPU_RE) + V(PD_CURR)*(V(KPD_RE)), V(PD_CURR)*V(KPD_FE) + V(PU_CURR)*(V(KPU_FE))) 

    print {$fho} "* Component capacitance\nCCOMP VEE OUT $c_comp\n";
    $c_comp = desuffix_number($c_comp);
    
    # fetch the appropriate I-V curves
    my @vi_pd = map { [ $_->[0], $_->[$typminmax_i] ] } @{$ibis->{model}{$model}{pulldown}[0]{table}};
    my @vi_pu = map { [ $_->[0], $_->[$typminmax_i] ] } @{$ibis->{model}{$model}{pullup}[0]{table}};

    my ($t_min, $t_max);
    for my $section_name ('rising_waveform', 'falling_waveform') {
        # select two V-t tables to use
        if ( @{$ibis->{model}{$model}{$section_name}} < 2 ) {
            die "At least two $section_name tables are needed\n";
        }
        if ( @{$ibis->{model}{$model}{$section_name}} > 2 ) {
            warn "The first two two $section_name tables will be used, others will be ignored\n";
        }
        my @vtw = (
            $ibis->{model}{$model}{$section_name}[0],
            $ibis->{model}{$model}{$section_name}[1],
        );

        # check that all V-t tables have same size
        if (@{$vtw[0]{table}} != @{$vtw[1]{table}}) {
            die "The $section_name tables have different numbers of rows in them\n";
        }

        # TODO: use Math::Spline to add extra interpolated datapoints in areas where V changes rapidly?
        
        my (@r_fixture, @v_fixture);
        for my $tablew (@vtw) {
            # check that all V-t tables have same timespan
            if (!defined $t_min) {
                $t_min=$tablew->{table}[0][0];
                $t_max=$tablew->{table}[-1][0];
            }
            else {
                my $dur = $t_max-$t_min;
                if (abs(($tablew->{table}[-1][0] - $tablew->{table}[0][0]) - $dur)/$dur > 0.01) {
                    die "V-t waveform tables have differing time spans\n";
                }
                if (abs($tablew->{table}[0][0] - $t_min)/$dur > 0.001) {
                    die "V-t waveform tables have differing start times\n";
                }
            }
            
            # get the fixture resistance and voltage for each table we are using
            push @r_fixture, desuffix_number($tablew->{r_fixture});
            my $v_fixture = $tablew->{v_fixture};
            $v_fixture = $tablew->{v_fixture_min} if $typminmax_i==2 && exists $tablew->{v_fixture_min};
            $v_fixture = $tablew->{v_fixture_max} if $typminmax_i==3 && exists $tablew->{v_fixture_max};
            push @v_fixture, desuffix_number($v_fixture);
            print "Using $section_name table with Vcc=$vcc, V_fixture=$v_fixture[-1], R_fixture=$r_fixture[-1]\n";
        }
        
        my @vt = map { $_->{table} } @vtw;
            
        # calculate the expected output voltage given the fixture load resistance/voltage, for each end of each V-t table
        my @v_loaded;
        for my $ti (0,1) {
            for my $logicstate (0,1) {
                my @iv; # column 0 = current, column 1 = voltage
                if ($logicstate) {
                    # driving high: pulldown transistor is off, pullup transistor is on
                    #~ print "  driving high into load pulling to V_fixture=$v_fixture[$ti]:";
                    for my $j (0..$#vi_pu) {
                        $iv[$j] = [ $vi_pu[$j][1], $vcc-$vi_pu[$j][0] ];
                    }
                }
                else {
                    # driving low: pulldown transistor is on, pullup transistor is off
                    #~ print "  driving low into load pulling to V_fixture=$v_fixture[$ti]:";
                    for my $j (0..$#vi_pu) {
                        $iv[$j] = [ $vi_pd[$j][1], $vi_pd[$j][0] ];
                    }
                }
                for my $j (0..$#iv) {
                    #~ printf " \tI=%.3f, V=%.3f", @{$iv[$j]};
                    $iv[$j][0] -= ($v_fixture[$ti] - $iv[$j][1])/$r_fixture[$ti];
                    #~ printf " \tI=%.3f, V=%.3f\n", @{$iv[$j]};
                }
                my $vout = lut_interpolate(\@iv, 0);
                #~ printf " Vout=%.3f\n", $vout;
                $v_loaded[$ti][$logicstate] = $vout;
            }
        }
        
        # make V-t splines
        my @vtspline = map {
            Math::Spline->new( 
                [ map { 1E9 * $_->[0]      } @{$vt[$_]} ], # X is time in ns
                [ map { $_->[$typminmax_i] } @{$vt[$_]} ] # Y is voltage in V
            ); 
        } 0, 1;
        # select time-points to use
        my @tns = map { 1E9 * $_->[0] } @{$vt[0]}; # take time points from first table, convert to ns
        my $dtd = $tns[1] - $tns[0]; # use this to track the smallest timestep, for now
        for my $ti (0,1) {
            add_x_points_to_make_linterp_match_spline(\@tns, $vtspline[$ti], $vcc/1000, \$dtd);
        }
        $dtd = 1E-9 * $dtd / 16; # $dtd is now the delta-t (in seconds) to use when calculating derivatives

        # compare the V-I voltage to the V-t voltage
        for my $ti (0,1) {
            for my $i (0, -1) { # look at beginning and end of V-t table
                my $logicstate = ($section_name eq 'falling_waveform') ^ ($i == -1);
                if ($logicstate) {
                    print "  driving high";
                }
                else {
                    print "  driving low";
                }
                print " into $r_fixture[$ti] Ohm load pulling to $v_fixture[$ti]V:";
                my $v_vi = $v_loaded[$ti][$logicstate];
                my $v_vt = $vt[$ti][$i][$typminmax_i];
                printf " Vout (from I-V tables)=%.3f, Vout (from V-t tables)=%.3f, difference = %d%% of Vcc\n", $v_vi, $v_vt, 100*($v_vt-$v_vi)/$vcc;
            }
        }

        # make K tables
        my (@ku, @kd);
        for my $t (@tns) {
            my @v = map { $vtspline[$_]->evaluate($t) } 0,1;
            #~ printf " \tV=%.3f,%.3f", @v;
            my @cap_current = map { $c_comp*( $vtspline[$_]->evaluate($t + 1E9*$dtd) - $v[$_] )/$dtd } 0,1; # I=C.dV/dt
            #~ printf " \tIc=%.3f,%.3f", @cap_current;
            my @current = map { ($v_fixture[$_] - $v[$_])/$r_fixture[$_] - $cap_current[$_] } 0,1;
            #~ printf " \tI=%.3f,%.3f", @current;
            my @ipu = map { lut_interpolate(\@vi_pu, $vcc-$v[$_]) } 0,1;
            #~ printf " \tIpu=%.3f,%.3f", @ipu;
            my @ipd = map { lut_interpolate(\@vi_pd, $v[$_]) } 0,1;
            #~ printf " \tIpd=%.3f,%.3f", @ipu;
            
            # do the matrix inversion
            my ($a, $b, $c, $d) = (
                $ipu[0], $ipd[0],
                $ipu[1], $ipd[1],
                );
            my $rdet = $a*$d - $b*$c;
            #~ printf " \trdet=%.3f", 1000*$rdet;
            # solve for Ks
            my $ku = ($d*$current[0] - $b*$current[1])/$rdet;
            my $kd = ($a*$current[1] - $c*$current[0])/$rdet;
            #~ printf " \tKu=%.3f,Kd=%.3f", $ku,$kd;
            
            #~ printf " \tI=%.3f,%.3f\n", map { $kd*$ipd[$_] + $ku*$ipu[$_] } 0,1;
            push @ku, $ku;
            push @kd, $kd;
        }
        #~ exit 0;
        
        # output the K tables
        for my $driver ('u', 'd') {
            print {$fho} '.SUBCKT LUT_K' . uc $driver;
            print {$fho} '_' . uc(substr $section_name, 0, 1) . "E 1 2 3 4\n";
            print {$fho} "E1 1 2 3 4 table = (\n";
            for my $i (0 .. $#{$vt[0]}) {
                printf {$fho} "+  %.4f,%.4f\n",
                    ($tns[$i]*1E-9 - $t_min)/($t_max-$t_min),
                    ($driver eq 'u' ? $ku[$i] : $kd[$i]),
                    ;
            }
            print {$fho} "+ )\n.ENDS\n";
        }
        
    }
    print {$fho} ".PARAM VT_DUR=".($t_max-$t_min)."\n";
    
    for my $pull ('pullup', 'pulldown') {
        my $table = $ibis->{model}{$model}{$pull}[0]{table};
        print {$fho} "\n* '$pull' table\n";
            print {$fho} '.SUBCKT ' . uc($pull) . " 1 2 3 4\n";
            print {$fho} "E1 1 2 3 4 table = (\n";
            for my $i (0..$#{$table}) {
                printf {$fho} "+  %s,%s\n",
                    $table->[$i][0],
                    $table->[$i][$typminmax_i],
                    ;
            }
            print {$fho} "+ )\n.ENDS\n";
        
    }
    
    print {$fho} ".ENDS\n";
}

sub add_x_points_to_make_linterp_match_spline {
    my ($x_ref, $spline, $y_acc, $minstep_ref) = @_;
    my $i=0;
    # an 'i' suffix means a value at X=$x_ref[$i]
    # a  'j' suffix means a value at X=$x_ref[$i+1]
    # a  'k' suffix means a value at X=half-way between
    my $xi = $x_ref->[0]; my $yi = $spline->evaluate($xi);
    my $xj = $x_ref->[1]; my $yj = $spline->evaluate($xj);
    while ( $i<$#{$x_ref} ) {
        #~ die 'ASSERT' unless $xj == $x_ref->[$i+1];
        #~ die 'ASSERT' unless $yi == $spline->evaluate($xi);
        #~ die 'ASSERT' unless $yj == $spline->evaluate($xj);
        my $xk = ($xi+$xj)/2;
        my $yk = $spline->evaluate($xk);
        my $ylint = ($yi+$yj)/2;
        if (abs($ylint - $yk) > $y_acc) {
            # too much difference - add an X point
            #~ print "adding a point between $xi and $xj at $xk\n";
            splice @{$x_ref}, $i+1, 0, $xk;
            # leave i unchanged, so that we consider this interval for further subdivision
            $xj = $xk; $yj = $yk;
        }
        else {
            #~ printf "difference at %.3f is %.3f\n", $xk, $ylint - $yk;
            ${$minstep_ref} = $xj-$xi if defined $minstep_ref && ${$minstep_ref} > $xj-$xi;
            $i++;
            $xi = $xj; $yi=$yj;
            $xj = $x_ref->[$i+1]; 
            $yj = $spline->evaluate($xj) if defined $xj;
        }
    }
}

sub lut_interpolate {
    my ($table, $col0_value) = @_;
    for my $i (0..$#{$table}-1) {
        if (($table->[$i][0] <= $col0_value && $table->[$i+1][0] > $col0_value)
            || ($table->[$i][0] >= $col0_value && $table->[$i+1][0] < $col0_value)) {
            # do a linear extrapolation to find Y coord of crossing point
            return $table->[$i][1] + ($table->[$i+1][1]-$table->[$i][1]) * ($col0_value-$table->[$i][0]) / ($table->[$i+1][0]-$table->[$i][0]);
        }
    }
    die "lut_interpolate failed to find X value\n";
}

sub desuffix_number {
    my @out = map {
        m/ \d ([kmunpf]?) (s|A|V|H|F|Ohm)? \z/xmsi ? substr($_,0,$-[1]) * $si_scaler{$1} : $_
    } strip_ws(@_);
    return wantarray ? @out : $out[0];
}

sub ibis_make_tables_numeric {
    my ($ibis) = @_;
    for my $section_name (grep { defined $section_type{$_}[1] && $section_type{$_}[1] eq 'table' } keys %section_type) {
        for my $model (keys %{$ibis->{model}}) {
            if (exists $ibis->{model}{$model}{$section_name}) {
                for my $tablew ( @{$ibis->{model}{$model}{$section_name}} ) {
                    my $table = $tablew->{table};
                    #~ print "found $section_name table for model $model\n";
                    #~ print Dump($table); exit 0;
                    for my $i (0..$#{$table}) {
                        @{$table->[$i]} = desuffix_number(@{$table->[$i]});
                    }
                    for my $k (keys %{$tablew}) {
                        #~ $tablew->{$k} = desuffix_number($tablew->{$k}) if !ref $tablew->{$k};
                    }
                }
            }
        }
    }
}

sub read_ibis {
    my ($infile, $outfile, $model_match) = @_;

    my $fhi;
    open $fhi, '<', $infile or die "Could not open '$infile' for reading: $!";

    my $ibis = { model => {} };
    my $store_in = $ibis;
    
    my $model_name = q{};
    my $section_name = q{};
    my @table_cols;
    
    while (defined (my $line = <$fhi>)) {
        next if ($line =~ /\A \s* \| /xmsi); # ignore comments
        chomp $line;
        if ($line =~ /\A \s* \[ (.*?) \] \s* (.*?) \z/xmsi) {
            my ($in_brackets, $after_brackets) = ($1, $2);
            $section_name = lc $in_brackets;
            $section_name =~ tr/ /_/;
            @table_cols = ();
            
            if ($section_name eq 'model') {
                $model_name = uc $after_brackets;
                if (exists $ibis->{model}{$model_name}) {
                    die "Duplicate model name, '$model_name'\n";
                }
                $ibis->{model}{$model_name} = {};
                $store_in = $ibis->{model}{$model_name};
                $after_brackets = q{};
                $section_name = 'model_parameters';
            }
            
            if ($model_name eq q{} || (!defined $model_match) || ($model_name =~ $model_match)) {
                if (defined $section_type{$section_name}[1] && $section_type{$section_name}[1] eq 'table') {
                    # start a new table
                    push @{$store_in->{$section_name}}, {table => []};
                }
                else {
                    _handle($after_brackets, $section_name, 0, $store_in, \@table_cols) if $after_brackets ne q{};
                }
            }
        }
        elsif ($model_name eq q{} || (!defined $model_match) || ($model_name =~ $model_match)) {
            _handle($line, $section_name, 1, $store_in, \@table_cols);
        }
    }
    
    return $ibis;
}

sub _handle {
    my ($input, $section_name, $body, $store_in, $table_cols) = @_;
    my $format = $section_type{$section_name}[$body];
    if (!defined $format) {
        warn "Don't know how to handle " . ($body ? 'body' : 'in-line') . " data for type '$section_name' section\n";
        return;
    }
    elsif ($format eq 'text') {
        if (exists $store_in->{$section_name}) {
            $store_in->{$section_name} .= ' ' .strip_ws($input);
        }
        else {
            $store_in->{$section_name} = strip_ws($input);
        }
    }
    elsif ($format eq 'table_headings') {
        @{$table_cols} = split ' ', $input;
    }
    elsif ($format eq 'mv') {
        my @v = split ' ', $input;
        $store_in->{$section_name} = \@v;
    }
    elsif ($format eq 'k_mv') {
        if (index($input, '=') >= 0) {
            my ($k, $v) = split /=/, $input, 2;
            $store_in->{$section_name}{strip_ws(lc $k)} = strip_ws($v);
        }
        else {
            my @v = split ' ', $input;
            my $k = lc shift @v;
            if (@{$table_cols}) {
                while (@{$table_cols} < @v) {
                    push @{$table_cols}, 'C'.(1+@{$table_cols});
                }
                for my $i (0..$#v) {
                    $store_in->{$section_name}{$k}{$table_cols->[$i]} = $v[$i] if defined $k;
                }
            }
            else {
                $store_in->{$section_name}{$k} = \@v if defined $k;
            }
        }
    }
    elsif ($format eq 'table') {
        if (index($input, '=') >= 0) {
            my ($k, $v) = split /=/, $input, 2;
            $store_in->{$section_name}[-1]{strip_ws(lc $k)} = strip_ws($v);
        }
        else {
            push @{$store_in->{$section_name}[-1]{table}}, [split ' ', $input];
        }
    }
    else { die; }
}

sub strip_ws {
    my @out;
    for my $x (@_) {
        my $in = $x;
        if (defined $in) {
            $in =~ s/\A \s+ //xms;
            $in =~ s/ \s+ \z//xms;
        }
        push @out, $in;
    }
    return wantarray ? @out : $out[0];
}