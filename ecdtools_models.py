# from pprint import pprint
import ecdtools
import sys
import matplotlib.pyplot as plt

if len(sys.argv) < 2:
    print('Specify an ibis file name, and optionally a model name')
    exit()

ibs_file = ecdtools.ibis.load_file(sys.argv[1], transform=True)

if len(sys.argv) < 3:
    print('Component names:')
    for name in ibs_file.component_names:
        print('    ' + name)

    print('Model names:')
    for name in ibs_file.model_names:
        model = ibs_file.get_model_by_name(name)
        print('    %s (%s)' % (name, model.model_type), end='')
        for aname in ('gnd_clamp', 'power_clamp', 'pullup', 'pulldown', 'ramp'):
            if getattr(model, aname) is not None:
                print(' ' + aname, end='')
        print()
        for aname in ('falling_waveforms', 'rising_waveforms'):
            for waveform in getattr(model, aname):
                print('        %s R=%.1f V=%.2f/%.2f/%.2f' % (aname[:-1], waveform.r_fixture, waveform.v_fixture.typical, waveform.v_fixture.minimum, waveform.v_fixture.maximum))
    exit()

model = ibs_file.get_model_by_name(sys.argv[2])
# vi = model.pullup
# plt.plot([r[0] for r in vi], [r[1] for r in vi], color="green", label="typ")
# plt.plot([r[0] for r in vi], [r[2] for r in vi], color="blue", label="min")
# plt.plot([r[0] for r in vi], [r[3] for r in vi], color="red", label="max")
# print(model.rising_waveforms)

# self.gnd_clamp = None
# self.power_clamp = None
# self.pullup = None
# self.pulldown = None
# self.ramp = None
# self.falling_waveforms = []
# self.rising_waveforms = []

# class Waveform(object):
#     def __init__(self):
#         self.r_fixture = None
#         self.v_fixture = TypMinMax()
#         self.table = WaveformTable()

# Name the plot after the model
plt.suptitle(sys.argv[2])

# Try to avoid text overlap
plt.subplots_adjust(hspace=0.5)

for n, plot in enumerate([ "pullup", "power_clamp", "pulldown", "gnd_clamp" ]):
    plt.subplot(3, 2, n + 1)
    vi = getattr(model, plot)
    if vi:
        plt.plot([r[0] for r in vi], [r[1] for r in vi], color="green", label="typ")
        plt.plot([r[0] for r in vi], [r[2] for r in vi], color="blue", label="min")
        plt.plot([r[0] for r in vi], [r[3] for r in vi], color="red", label="max")
        ymin, ymax = plt.ylim()
        extra = (ymax - ymin) * 0.01
        plt.ylim(ymin-extra, ymax+extra)
    plt.title(plot)

    # A little complex logic to put legend and axis in sane places.
    if n > 1:
        plt.xlabel('Volts')
        plt.legend(loc=4)
    else:
        plt.legend()
    if not n & 1:
        plt.ylabel('Amps')
    plt.grid()

for n, plot in enumerate([ 'rising_waveforms', 'falling_waveforms' ]):
    plt.subplot(3, 2, n + 5)
    labels = [ "typ", "min", "max" ]
    for wf in getattr(model, plot):
        plt.plot([r[0] for r in wf.table.samples], [r[1] for r in wf.table.samples], color="green", label=labels[0])
        plt.plot([r[0] for r in wf.table.samples], [r[2] for r in wf.table.samples], color="blue", label=labels[1])
        plt.plot([r[0] for r in wf.table.samples], [r[3] for r in wf.table.samples], color="red", label=labels[2])
        # Don't make duplicate labels for subsequent waveforms.
        labels = [ None, None, None ]
    ymin, ymax = plt.ylim()
    extra = (ymax - ymin) * 0.01
    plt.ylim(ymin-extra, ymax+extra)
    plt.title(plot)
    plt.xlabel('Time (s)')

    # A little complex logic to put legend and axis in sane places.
    if not n & 1:
        plt.ylabel('Volts')
        plt.legend(loc=4)
    else:
        plt.legend()
    plt.grid()

plt.show()

