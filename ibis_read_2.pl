#!/usr/bin/perl

# extract all the info from an IBIS file

use Hardware::ParseIBIS;
#~ use YAML;
use Math::Spline;

use strict;
use warnings;


#~ my $vcc = desuffix_number('3.60V'); print "$vcc\n"; exit 0;

#~ my $ibis = read_ibis('D:/t/spice/82375sb.ibs',undef, qr/\A PCEBB12211A0S2AZZZZC \z/xms);
#~ print Dump($ibis); exit 0;
#~ print Dump($ibis->{model}{LVCMOS33_F_4_LR_33}{'rising_waveform'});

if (1) {
    my $ibis = Hardware::ParseIBIS->new({infile => 'R:/Hardware/Product Design data/PCB Designs/EtherDIO/90ED593R2/Documents/Protection/TPD2E2U06.ibs'});
    $ibis->make_tables_numeric();
    #~ print Dump($ibis);

    my $outfile = 'R:/Hardware/Product Design data/PCB Designs/EtherDIO/90ED593R2/Documents/Protection/TPD2E2U06.lib';
    my $fho;
    open $fho, '>', $outfile or die "Could not open '$outfile' for writing: $!";
    $ibis->make_spice_models($fho, 'IN_50A', 'IN_50A', 1);
    close $fho;
}

if (0) {
    my $ibis = read_ibis('xc3sa_ft256.ibs',undef, qr/\A LVCMOS33_F_[48]_LR_33 \z/xms);
    ibis_make_tables_numeric($ibis);

    save_vt_tables($ibis, 'LVCMOS33_F_4_LR_33', 1, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/xc3sa_vt_'    , 2.05E-9, 17.55E-9);
    save_vt_tables($ibis, 'LVCMOS33_F_4_LR_33', 2, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/xc3sa_vt_min_', 2.05E-9, 17.55E-9);
    save_vt_tables($ibis, 'LVCMOS33_F_4_LR_33', 3, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/xc3sa_vt_max_', 2.05E-9, 17.55E-9);

    my $outfile = 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/xs3a.lib';
    my $fho;
    open $fho, '>', $outfile or die "Could not open '$outfile' for writing: $!";
    for my $ds (4, 8) {
        make_spice_models($fho, $ibis, "LVCMOS33_F_${ds}_LR_33", "C33F${ds}"    , 1);
        make_spice_models($fho, $ibis, "LVCMOS33_F_${ds}_LR_33", "C33F${ds}_MIN", 2);
        make_spice_models($fho, $ibis, "LVCMOS33_F_${ds}_LR_33", "C33F${ds}_MAX", 3);
    }
    close $fho;
}

if (0) {
    my $ibis = read_ibis('lm3s9b90_lqfp_v0p1.ibs',undef, qr{\A I/O_[48] \z}xms);
    ibis_make_tables_numeric($ibis);
    
    save_vt_tables($ibis, 'I/O_4', 1, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/uc_vt_'    , 2.05E-9, 28.05E-9);
    save_vt_tables($ibis, 'I/O_4', 2, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/uc_vt_min_', 2.05E-9, 28.05E-9);
    save_vt_tables($ibis, 'I/O_4', 3, 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/uc_vt_max_', 2.05E-9, 28.05E-9);

    my $outfile = 'R:/Projects/Ethernet to Serial/Notes/FPGA/spice/lm3s9b90.lib';
    my $fho;
    open $fho, '>', $outfile or die "Could not open '$outfile' for writing: $!";
    for my $ds (4, 8) {
        make_spice_models($fho, $ibis, "I/O_${ds}", "IO${ds}"    , 1);
        make_spice_models($fho, $ibis, "I/O_${ds}", "IO${ds}_MIN", 2);
        make_spice_models($fho, $ibis, "I/O_${ds}", "IO${ds}_MAX", 3);
    }
    close $fho;
}
