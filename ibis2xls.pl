#!/usr/bin/perl

# extract all the t-V and V-I tables from an IBIS file and export them to sheets in an Excel spreadsheet

use Spreadsheet::WriteExcel;
use Spreadsheet::WriteExcel::Utility qw( xl_range_formula );
#~ use YAML;

use strict;
use warnings;

#~ split_ibis('lm3s9b90_lqfp_v0p1.ibs','lm3s9b90_lqfp_v0p1.xls');
#~ split_ibis('lm3s9b90_lqfp_v0p1.ibs','lm3s9b90_lqfp_v0p1.xls', qr{\A I/O_4 }xms);
split_ibis('xc3sa.ibs','spartan3a.xls', qr/\A LVCMOS33_F_4_LR_33 \z/xms);
#~ split_ibis('xc3sa.ibs',undef, qr/\A LVCMOS33_F_4_LR_33 \z/xms);
#~ split_ibis('D:/t/spice/82375sb.ibs','D:/t/spice/82375sb.xls', qr/\A PCEBB12211A0S2AZZZZC \z/xms);

#~ split_ibis('lm3s9b90_lqfp_v0p1.ibs',undef, qr{\A I/O_ }xms);
#~ split_ibis('xc3sa.ibs',undef, qr/\A LVCMOS33_F .* _33 \z/xms);

sub split_ibis {
    my ($infile, $outfile, $model_match) = @_;

    my $fhi;
    (-f $infile && -r $infile) || die "'$infile' is not readable";
    open $fhi, '<', $infile || die "Could not open '$infile' for reading: $!";

    my $workbook  = Spreadsheet::WriteExcel->new($outfile) if defined $outfile;

    my $model_name = q{};
    my $section_name = q{};
    my $section_contents = q{};
    
    while (defined (my $line = <$fhi>)) {
        chomp $line;
        if ($line =~ /\A \s* \[ (.*?) \] \s* (.*?) \z/xmsi) {
            my ($in_brackets, $after_brackets) = ($1, $2);
            process_section($model_name, $section_name, $section_contents, $workbook) if (!defined $model_match) || ($model_name =~ $model_match);
            $section_name = lc $in_brackets;
            $model_name = $after_brackets if $section_name eq 'model';
            $section_contents = q{};
        }
        else {
            $section_contents .= $line . "\n";
        }
    }
}

sub process_section {
    my ($model_name, $section_name, $section_contents, $workbook) = @_;
    print "model '$model_name', section '$section_name': " . length($section_contents) . " bytes\n";
    return unless $model_name ne q{} && $section_name =~ /\A 
        (
          falling [ ] waveform
        | rising [ ] waveform
        | pulldown 
        | pullup 
        | gnd [ _] clamp 
        | power [ _] clamp 
    ) \z/xms;
    
    my %si_scaler = (
        q{} => 1,
        m => 1E-3,
        u => 1E-6,
        n => 1E-9,
        p => 1E-12,
        f => 1E-15,
    );
    
    my $worksheet_name = "$model_name $section_name";
    $section_contents = "\n" . $section_contents;
    if ($section_contents =~ / \v V_fixture \s* = \s* (\S*) \v /xmsi) {
        $worksheet_name .= ' ' . $1;
    }
    substr $section_contents,0,1,q{}; # remove the initial "\n" we just added
    $worksheet_name =~ tr{/\\\[\]:?}{__();_}; # remove invalid characters []:*?/\ from worksheet name
    $worksheet_name =~ s{ \s+ waveform \b }{}xms;
    $worksheet_name =~ s{ (\d [.] \d) 0+ \b}{$1}xms;
    $worksheet_name = substr($worksheet_name, 0, 31) if length $worksheet_name>31;

    my $worksheet;
    if (defined $workbook) {
        print "Writing worksheet '$worksheet_name'\n"; binmode STDOUT;
        $worksheet = $workbook->add_worksheet($worksheet_name);
    }
    
    my @headings;
    my @data;
    my $row=1;
    my $table_start = 1;
    my $table_end = 1;
    for my $line (split /\n/, $section_contents) {
        last if $line =~ s/\A \s* [|] //xms && @data; # remove comment indicator from start of line
        $line = strip_ws($line);
        if ($line =~ /\A [= ]* \z/xmsi) {
            # ignore this content-free line
        }
        elsif ($line =~ /\A \s* (.*?) \s* = \s* (.*?) \s* \z/xmsi) {
            #~ print "param line: $line\n";
            $worksheet->write( 'A'.($row++), [strip_ws(split /=/, $line)] ) if defined $worksheet;
            $table_start = $row;
        }
        elsif ($row == $table_start) {
            # looks like heading row of data table
            #~ print "heading line: $line\n";
            @headings = strip_ws(split /\s+/, $line);
            @data = ();
            $worksheet->write( 'A'.$row, \@headings ) if defined $worksheet;
            $row++;
        }
        else {
            #~ print "table line: $line\n";
            $table_end = $row;
            my @rowdata = map {
                m/ \d ([munpf]?) [sAV] \z/xmsi ? substr($_,0,$-[1]) * $si_scaler{$1} : $_;
            } strip_ws(split /\s+/, $line);
            $worksheet->write( 'A'.$row, \@rowdata ) if defined $worksheet;
            $row++;
            push @data, \@rowdata;
        }
    }
    
    return if $table_end<=$table_start;
    
    if ($section_name !~ /clamp\z/xms) {
        # do some analysis of the data in the table
        # assumed to be 3 columns: X, Y1, Y2, Y3
        #~ print Dump(\@data); exit 0;
        my $table_type = $section_name =~ / \b waveform \b /xmsi ? 'VT' : 'IV';
        for my $series (1..3) {
            # find the points at 25% and 75% of the edge
            my $ymin = $data[0][$series];
            my $ymax = $data[-1][$series];
            if ($table_type eq 'IV') {
                $ymin = 0;
                $ymax = $data[0.6666*$#data][$series];
            }
            ($ymin, $ymax) = ($ymax, $ymin) if $ymin>$ymax;
            my @ymeas = (
                $ymin + 0.25*($ymax - $ymin),
                $ymin + 0.75*($ymax - $ymin),
            );
            my @xmeas;
            for my $i (0..$#data-1) {
                for my $meas (0..$#ymeas) {
                    if (($data[$i][$series] <= $ymeas[$meas] && $data[$i+1][$series] > $ymeas[$meas])
                        || ($data[$i][$series] >= $ymeas[$meas] && $data[$i+1][$series] < $ymeas[$meas])) {
                        warn "multiple crossings of measurement point $headings[$series]=$ymeas[$meas] found" if defined $xmeas[$meas];
                        # do a linear extrapolation to find X coord of crossing point
                        #~ print "Searching for Y=$ymeas[$meas] between ($data[$i][0],$data[$i][$series]) and ($data[$i+1][0],$data[$i+1][$series])";
                        $xmeas[$meas] = $data[$i][0] + ($data[$i+1][0]-$data[$i][0]) * ($ymeas[$meas]-$data[$i][$series]) / ($data[$i+1][$series]-$data[$i][$series]);
                        #~ print " -> $xmeas[$meas]\n";
                    }
                }
            }
            print "$model_name $section_name $headings[$series] ";
            if ($table_type eq 'IV') {
                printf "slope resistance (V=%.3f - %.3fV) = \t%.1f \tOhms\n", $xmeas[0], $xmeas[1], abs(($xmeas[1]-$xmeas[0])/($ymeas[1]-$ymeas[0]));
            }
            else {
                printf "edge time = \t%.3f \tns\n", 2E9 * abs($xmeas[1]-$xmeas[0]);
            }
        }
        binmode STDOUT;
    }
    
    return if !defined $workbook;
    my $chart = $workbook->add_chart( type => 'line', embedded => 1 );

    # Configure the chart.
    for my $series (1..3) {
        $chart->add_series(
            name       => $headings[$series],
            categories => xl_range_formula( $worksheet->get_name(), $table_start, $table_end, 0, 0 ),
            values     => xl_range_formula( $worksheet->get_name(), $table_start, $table_end, $series, $series ),
        );
    }

    # Add a chart title and some axis labels.
    $chart->set_title ( name => "$model_name $section_name" );
    $chart->set_x_axis( name => $headings[0] );
    if ($headings[1] =~ /\A I[(] /xms ) {
        $chart->set_y_axis( name => 'Current' );
    }
    if ($headings[1] =~ /\A V[(] /xms ) {
        $chart->set_y_axis( name => 'Voltage' );
    }

    # Insert the chart into the a worksheet.
    $worksheet->insert_chart('E1', $chart, 10, 5, 1.5, 2.0);
}

sub strip_ws {
    my @out;
    for my $in (@_) {
        $in =~ s/\A \s+ //xms;
        $in =~ s/ \s+ \z//xms;
        push @out, $in;
    }
    return wantarray ? @out : $out[0];
}